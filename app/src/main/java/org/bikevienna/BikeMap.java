package org.bikevienna;

import android.content.Context;
import android.graphics.Point;
import android.view.WindowManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.algo.NonHierarchicalViewBasedAlgorithm;
import org.bikevienna.cluster.IconClusterRenderer;
import org.bikevienna.map.MapArray;
import org.bikevienna.map.MapObject;
import org.bikevienna.map.marker.MapMarker;
import org.bikevienna.map.route.BikeRoad;
import org.bikevienna.map.route.MapRoute;

import java.util.Collection;

/*
 * Contains everything related to GoogleMap
 */

public class BikeMap{

    public static final LatLng VIENNA_COOR = new LatLng(48.2082, 16.3738);
    public static final float MIN_ZOOM = 13.0f;

    private static GoogleMap mMap;
    private static ClusterManager<MapMarker> mClusterManager;

    public static void init(GoogleMap map){
        mMap = map;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(VIENNA_COOR, MIN_ZOOM));
        mMap.setMinZoomPreference(MIN_ZOOM);
    }

    public static void initCluster(Context ctx){
        mClusterManager = new ClusterManager<>(ctx, mMap);

        Point size = getDisplaySize(ctx);
        mClusterManager.setAlgorithm(new NonHierarchicalViewBasedAlgorithm<MapMarker>(size.x/3, size.y/3));
        mClusterManager.setRenderer(new IconClusterRenderer(ctx, mMap, mClusterManager));

        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
    }

    public static ClusterManager<MapMarker> getClusterManager(){
        return mClusterManager;
    }
    public static GoogleMap getMap(){
        return mMap;
    }

    private static Point getDisplaySize(Context ctx){
        Point result = new Point();
        WindowManager winManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        if(winManager != null)
            winManager.getDefaultDisplay().getSize(result);

        return result;
    }
}
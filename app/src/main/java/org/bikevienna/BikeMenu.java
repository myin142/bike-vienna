package org.bikevienna;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import org.bikevienna.map.MapArray;
import org.bikevienna.map.marker.ParkingMarkers;
import org.bikevienna.map.marker.PumpMarkers;
import org.bikevienna.map.route.BikeRoad;

/*
 * Contains everything related to the Navigation Menu
 */

public class BikeMenu{

    private static Menu mMenu;
    private static MapArray[] mMarkList;
    private static boolean[] mMarkState;

    public static void init(NavigationView nav, Activity activity){
        mMenu = nav.getMenu();
        mMarkState = new boolean[mMenu.size()];

        nav.setItemIconTintList(null);

        NavigationListener navListener = new NavigationListener();
        nav.setNavigationItemSelectedListener(navListener);
        for(int i = 0; i < mMenu.size(); i++){
            mMenu.getItem(i).getActionView().setOnClickListener(navListener);
        }

        mMarkList = new MapArray[]{
                new ParkingMarkers(activity),
                new PumpMarkers(activity),
                new BikeRoad(activity)
        };
    }

    public static void initMarkers(){
        for(int i = 0; i < mMarkList.length; i++){
            if(mMarkState[i]){
                mMarkList[i].loadMapObjects();
            }
        }
    }

    public static int getChangedIndex(){
        for(int i = 0; i < mMenu.size(); i++){
            CheckBox checkBox = (CheckBox) mMenu.getItem(i).getActionView();
            if(mMarkState[i] != checkBox.isChecked()){
                return i;
            }
        }
        return -1;
    }

    public static void load(boolean[] savedState){
        if(savedState.length == 0 || savedState.length != mMarkState.length) return;

        mMarkState = savedState;
        syncMarks();
    }

    public static boolean[] getState(){
        return mMarkState;
    }

    public static MapArray getMarkerArrayById(int id){
        return mMarkList[id];
    }

    private static void syncMarks(){
        for(int i = 0; i < mMenu.size(); i++){
            CheckBox checkBox = (CheckBox) mMenu.getItem(i).getActionView();
            checkBox.setChecked(mMarkState[i]);
        }
    }

    /*
     *  Listener for Navigation Menu Items clicked
     */
    private static class NavigationListener implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

        // Non-Checkbox Click
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            View view = item.getActionView();
            toggleCheckBox(view);
            onClick(view);
            return false;
        }

        @Override
        public void onClick(View view) {
            int index = BikeMenu.getChangedIndex();
            if(index != -1){
                MapArray mapItem = BikeMenu.getMarkerArrayById(index);
                if(mapItem.isLoading()){
                    toggleCheckBox(view);
                    return;
                }

                // Update Menu State
                CheckBox checkBox = (CheckBox) view;
                BikeMenu.getState()[index] = checkBox.isChecked();

                if(!checkBox.isChecked() && !mapItem.initialize()){
                    mapItem.removeFromMap();
                }else if(checkBox.isChecked()){
                    if(mapItem.initialize()){
                        mapItem.loadMapObjects();
                    }else{
                        mapItem.addToMap();
                    }
                }
            }
        }

        private void toggleCheckBox(View view){
            CheckBox checkBox = (CheckBox) view;
            checkBox.setChecked(!checkBox.isChecked());
        }
    }
}
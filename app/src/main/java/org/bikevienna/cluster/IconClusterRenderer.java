package org.bikevienna.cluster;

import android.content.Context;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import org.bikevienna.map.marker.MapMarker;

public class IconClusterRenderer extends DefaultClusterRenderer<MapMarker> {

    public IconClusterRenderer(Context ctx, GoogleMap map, ClusterManager<MapMarker> clusterManager){
        super(ctx, map, clusterManager);
    }

    // Render own icon for each Marker
    @Override
    protected void onBeforeClusterItemRendered(MapMarker item, MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions);
        markerOptions.icon(item.getIcon());
    }

}
package org.bikevienna.map;

import android.app.Activity;
import android.os.AsyncTask;

import org.bikevienna.Debug;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

/*
 * Superclass for all Containers
 *
 * Contains all information about one item in NavigationView Menu
 */

public abstract class MapArray{

    protected enum Delimiter{
        COMMA, SEMICOLON
    }

    // Overrideable Field
    protected abstract String metaUrl();
    protected int resIndex() { return 1; }
    protected Charset charset(){ return StandardCharsets.UTF_8; }
    protected Delimiter delimiter() { return Delimiter.COMMA; }
    protected int skipLines() { return 1; }

    protected MapObject[] mMapObjects;
    private Activity mActivity;
    private boolean loading;

    public MapArray(Activity activity){
        mActivity = activity;
    }

    // Initial Marker Creation
    public void loadMapObjects(){
        loading = true;
        new FetchMapObjects(this).execute();
    }

    public boolean initialize(){
        return mMapObjects == null;
    }
    public boolean isLoading() { return loading; }

    // Get String surrounded by 'start' and 'end' String
    protected String getStringBetween(String str, String start, String end){
        return str.substring(str.indexOf(start) + 1, str.lastIndexOf(end));
    }

    // Reformat to valid String
    protected String processString(String str){
        return str.replace("\"", "");
    }

    // Create MapMarker from Line
    protected abstract MapObject prepareMarker(String[] values);

    public abstract void addToMap();
    public abstract void removeFromMap();

    private static class FetchMapObjects extends AsyncTask<Void, Void, MapObject[]> {

        private WeakReference<MapArray> outerRef;

        private FetchMapObjects(MapArray outer){
            outerRef = new WeakReference<>(outer);
        }

        @Override
        protected MapObject[] doInBackground(Void... voids) {
            ArrayList<MapObject> items = new ArrayList<>();
            MapArray outer = outerRef.get();

            try {
                JSONObject metaData = new JSONObject(readStringFromURL(outer.metaUrl(), outer.charset()));
                String apiUrl = ((JSONObject) metaData.getJSONObject("result").getJSONArray("resources").get(outer.resIndex())).getString("url");

                BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(apiUrl).openStream()));

                // Skip Lines
                for (int i = 0; i < outer.skipLines(); i++) {
                    reader.readLine();
                }

                String line;
                while ((line = reader.readLine()) != null) {
                    items.add(outer.prepareMarker(splitCSV(line, outer.delimiter())));
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            MapObject[] result = new MapObject[items.size()];
            items.toArray(result);
            return result;
        }

        @Override
        protected void onPostExecute(MapObject[] mapObjects) {
            super.onPostExecute(mapObjects);

            Debug.log("Post Execute: " + mapObjects.length);
            final MapArray outer = outerRef.get();
            outer.mMapObjects = mapObjects;

            outer.mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    outer.addToMap();
                    outer.loading = false;
                }
            });
        }

        private static String[] splitCSV(String line, Delimiter delim){
            char delimChar;
            switch (delim){
                case COMMA: delimChar = ',';
                    break;
                case SEMICOLON: delimChar = ';';
                    break;
                default: delimChar = ',';
            }

            return line.split(delimChar + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
        }

        private static String readStringFromURL(String requestURL, Charset charset) throws IOException {
            try (Scanner scanner = new Scanner(new URL(requestURL).openStream(), charset.name()))
            {
                scanner.useDelimiter("\\A");
                return scanner.hasNext() ? scanner.next() : "";
            }
        }
    }
}
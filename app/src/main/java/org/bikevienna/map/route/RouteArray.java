package org.bikevienna.map.route;

import android.app.Activity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.bikevienna.BikeMap;
import org.bikevienna.map.MapArray;
import org.bikevienna.map.MapObject;

/*
 * Superclass for Route Containers
 */

abstract class RouteArray extends MapArray{
    public RouteArray(Activity activity) {
        super(activity);
    }

    @Override
    public void addToMap() {
        GoogleMap map = BikeMap.getMap();
        for(MapObject mapObject : mMapObjects){
            MapRoute route = (MapRoute) mapObject;
            Polyline line = map.addPolyline(new PolylineOptions().addAll(route.getPoints()));
            line.setVisible(false);
            route.setPolyline(line);
        }
    }

    @Override
    public void removeFromMap() {
        for(MapObject route : mMapObjects){
            ((MapRoute) route).removePolyline();
        }
    }
}
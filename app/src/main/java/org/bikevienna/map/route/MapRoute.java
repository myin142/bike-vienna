package org.bikevienna.map.route;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import org.bikevienna.map.MapObject;

import java.util.ArrayList;

/*
 * Route class for Polylines
 */

public class MapRoute implements MapObject{
    private ArrayList<LatLng> mPoints;
    private float mWidth;
    private PatternItem mPattern;
    private Polyline mPolyline;

    public MapRoute(ArrayList<LatLng> points, float width, PatternItem pattern){
        mPoints = points;
        mWidth = width;
        mPattern = pattern;
    }
    public MapRoute(ArrayList<LatLng> points, float width){
        this(points, width, null);
    }
    public MapRoute(ArrayList<LatLng> points, PatternItem pattern){
        this(points, 0.0f, pattern);
    }
    public MapRoute(ArrayList<LatLng> points){
        this(points, 0.0f, null);
    }

    public void setPolyline(Polyline line){
        mPolyline = line;
    }
    public void removePolyline(){
        if(mPolyline != null)
            mPolyline.remove();
    }

    public ArrayList<LatLng> getPoints(){
        return mPoints;
    }
}
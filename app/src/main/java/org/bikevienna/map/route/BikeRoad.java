package org.bikevienna.map.route;

import android.app.Activity;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/*
 * Route Container for Bike Routes
 */

public class BikeRoad extends RouteArray{
    protected String metaUrl(){ return "https://www.data.gv.at/katalog/api/3/action/package_show?id=5e6175cd-dc44-4b32-a64a-1ac4239a6e4a"; }

    public BikeRoad(Activity activity) {
        super(activity);
    }

    @Override
    protected MapRoute prepareMarker(String[] values){
        // Get all Point for Polyline
        ArrayList<LatLng> points = new ArrayList<>();
        getPointRec(values[1], points);

        return new MapRoute(points);
    }

    // Getting Point Recursively for MultiLineStrings
    private void getPointRec(String line, ArrayList<LatLng> array){
        String newLine = getStringBetween(line, "(", ")");
        if(containsBrackets(newLine)){
            //String pattern = String.valueOf(Pattern.compile(",(?![^()]*\\))"));
            String[] split = newLine.split(",(?![^()]*\\))");
            for(String item : split){
                getPointRec(item, array);
            }
        }else{
            String[] split = newLine.split(",");
            for(String item : split){
                String[] point = item.trim().split(" ");
                array.add(new LatLng(Double.parseDouble(point[1]), Double.parseDouble(point[0])));
            }
        }
    }

    private boolean containsBrackets(String str){
        return (str.contains("(") && str.contains(")"));
    }
}
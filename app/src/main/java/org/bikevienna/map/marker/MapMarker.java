package org.bikevienna.map.marker;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import org.bikevienna.map.MapObject;

/*
 * Marker Class for ClusterManager
 */

public class MapMarker implements ClusterItem, MapObject {

    private LatLng mPosition;
    private String mTitle;
    private String mSnippet;
    private BitmapDescriptor mIcon;

    public MapMarker(LatLng latlng, String title, String snippet, BitmapDescriptor icon){
        mPosition = latlng;
        mTitle = title;
        mSnippet = snippet;
        mIcon = icon;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }

    public BitmapDescriptor getIcon() {
            return mIcon;
    }
}
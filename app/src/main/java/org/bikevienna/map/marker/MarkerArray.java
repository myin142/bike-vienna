package org.bikevienna.map.marker;

import android.app.Activity;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.maps.android.clustering.ClusterManager;

import org.bikevienna.BikeMap;
import org.bikevienna.map.MapArray;
import org.bikevienna.map.MapObject;

/*
 * Superclass for Marker Containers
 */

abstract class MarkerArray extends MapArray{

    // Variables specific for each Marker Container
    protected abstract BitmapDescriptor icon();

    public MarkerArray(Activity activity) {
        super(activity);
    }

    @Override
    public void addToMap() {
        ClusterManager<MapMarker> clusterManager = BikeMap.getClusterManager();
        for(MapObject marker : mMapObjects){
            clusterManager.addItem((MapMarker) marker);
        }
        clusterManager.cluster();
    }

    @Override
    public void removeFromMap() {
        ClusterManager<MapMarker> clusterManager = BikeMap.getClusterManager();
        for(MapObject marker: mMapObjects){
            clusterManager.removeItem((MapMarker) marker);
        }
        clusterManager.cluster();
    }
}
package org.bikevienna.map.marker;

import android.app.Activity;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import org.bikevienna.R;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/*
 * Marker Container for Bike Pumps
 */

public class PumpMarkers extends MarkerArray{

    protected int resIndex() { return 0; }
    protected String metaUrl(){ return "https://www.data.gv.at/katalog/api/3/action/package_show?id=24d8ecf0-4cda-4597-9f22-c2a77a91c425"; }
    protected BitmapDescriptor icon(){ return BitmapDescriptorFactory.fromResource(R.drawable.bike_pump); }
    protected Charset charset(){ return StandardCharsets.ISO_8859_1; }

    protected Delimiter delimiter(){ return Delimiter.SEMICOLON; }
    protected int skipLines(){ return 2; }

    public PumpMarkers(Activity activity) {
        super(activity);
    }

    @Override
    protected MapMarker prepareMarker(String[] values) {
        double lat = Long.parseLong(values[1]) / Math.pow(10d, (values[1].length()-2));
        double lng = Long.parseLong(values[2]) / Math.pow(10d, (values[2].length()-2));

        return new MapMarker(new LatLng(lat, lng), processString(values[0]), null, icon());
    }
}
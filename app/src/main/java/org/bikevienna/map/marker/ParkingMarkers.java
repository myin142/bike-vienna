package org.bikevienna.map.marker;

import android.app.Activity;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import org.bikevienna.R;

/*
 * Marker Container for Bike Parking
 */

public class ParkingMarkers extends MarkerArray {

    protected String metaUrl(){ return "https://www.data.gv.at/katalog/api/3/action/package_show?id=97ef14eb-f280-48a7-96c0-df03859b06c2"; }
    protected BitmapDescriptor icon(){ return BitmapDescriptorFactory.fromResource(R.drawable.bike_park); }

    public ParkingMarkers(Activity activity){
        super(activity);
    }

    // Get Parking Type from Number
    private static String getTypeName(int type) {
        switch(type){
            case 0: return "Fahrradabstellanlage";
            case 1: return "Überdachte Fahrradabstellanlage";
            case 3: return "Fahrradbox/-garage";
        }
        return "Invalid Type";
    }

    @Override
    protected MapMarker prepareMarker(String[] values){
        String[] latlong = getStringBetween(values[1], "(", ")").split(" ");

        String snippet = ParkingMarkers.getTypeName(Integer.parseInt(values[7])) + " - Anzahl: " + values[6];

        return new MapMarker(new LatLng(
                Double.parseDouble(latlong[1]),
                Double.parseDouble(latlong[0])
        ), processString(values[5]), snippet, icon());
    }
}

package org.bikevienna.map;

import android.content.Context;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import org.bikevienna.BikeMap;
import org.bikevienna.BikeMenu;

/*
 * Listener for Google Map Ready State
 */

public class MapListener implements OnMapReadyCallback{

    private Context mContext;

    public MapListener(Context ctx){
        mContext = ctx;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        BikeMap.init(googleMap);
        BikeMenu.initMarkers();
        BikeMap.initCluster(mContext);
    }
}
package org.bikevienna;

import android.content.SharedPreferences;

/*
 * Used to save/load the last state of the Navigation Menu Checkboxes
 */

class Storage {

    private static final String MARK_STORAGE = "displayMarker";
    private static SharedPreferences mPreferences;

    public static void init(SharedPreferences pref){
        mPreferences = pref;
    }

    public static void storePreference(boolean[] values){
        SharedPreferences.Editor prefEdit = mPreferences.edit();
        prefEdit.putInt(MARK_STORAGE + "_size",  values.length);

        for(int i = 0; i < values.length; i++){
            prefEdit.putBoolean(MARK_STORAGE + "_" + i, values[i]);
        }
        prefEdit.apply();
    }

    public static boolean[] loadPreference(){
        boolean[] result = new boolean[mPreferences.getInt(MARK_STORAGE + "_size", 0)];

        for(int i = 0; i < result.length; i++){
            result[i] = mPreferences.getBoolean(MARK_STORAGE + "_" + i, false);
        }
        return result;
    }
}
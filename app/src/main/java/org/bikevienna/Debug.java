package org.bikevienna;

import android.util.Log;

public class Debug{
    private static long startTime = 0;
    private static long time = 0;

    public static void startTimer(){
        startTime = System.currentTimeMillis();
    }

    public static void stopTimer(){
        if(startTime != 0L){
            time += System.currentTimeMillis() - startTime;
            startTime = 0;
        }
    }

    public static void showTimerResult(String prefix){
        long res = time / 1000;
        Log.d(MapsActivity.MAP_LOG, prefix + ": " + res + "s");

        time = 0;
    }

    public static void log(String str){
        Log.d(MapsActivity.MAP_LOG, (str == null) ? "Null" : str);
    }
}
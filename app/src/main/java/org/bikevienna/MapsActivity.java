package org.bikevienna;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.google.android.gms.maps.SupportMapFragment;

import org.bikevienna.map.MapListener;

public class MapsActivity extends AppCompatActivity{

    public static final String MAP_LOG = "MapActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Storage.init(getPreferences(Context.MODE_PRIVATE));
        BikeMenu.init((NavigationView) findViewById(R.id.nav), this);
        BikeMenu.load(Storage.loadPreference());

        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(new MapListener(this));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Storage.storePreference(BikeMenu.getState());
    }
}
